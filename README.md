# mtr

Library for the MTR (Match-Test-Replace) data modification strategy licensed under GPLv3+.

MTR is in the same category of concepts as Find-Replace, except designed for the purposes of preserving the original data as much as is possible when the same meaning is expressed even if in slightly different raw formats.

For instance, imagine a config file containing key/value pairs for a program, that also contains useful comments. If you were to autogenerate this configuration file (or often, modify it in place, to for instance preserve whatever pairs are not specifically being changed), a typical method would be the parsing of the file into an internal structure (stripping away the comments and other formatting), then performing modifications, then rewriting the file once again.

Naturally this loses all the formatting and comments, in most cases. So what if you want to preserve that and essentially make the minimum changes to a file or any other piece of data to contain certain properties? The way to do that (or at least, the way this library does it) is to locate ("Match") various parts of the data, then for each Match, see if it satisfies the requirements ("Test") - for instance if the Match of a key contains an appropriate value. If it does, then everything is left as normal. If not, then the region is Replaced with an appropriate value.

On it's own, this already adds some flexibility. Adding the ability to combine and interleave MTR specification enables near-minimal data modification by only changing the parts of a region that are invalid, and having a Test result be composed of all sub-Test-results.

### On the matter of defaults and present-and-absent matches.
When the format of some data involves a notion of defaults, things can become slightly more complicated. Continuing the example of a key/value mapping, there are several ways unspecified kv pairs can be handled. In the case of a default value existing *and being desired as the actual value*, it may be preferrable for any pattern found to have a replacement value of, essentially, deletion. 

Other times, it may be desired to ensure that the given *match* is present in the data even if it does not exist currently (if, for instance, a default is being used). In such a case, the *replacement* must be inserted if the match is otherwise unfound. 

Most often - such as in simple MTR specifiers composed into other ones - it is desired that matching and replacing occur in a simple manner free of the wider context of "Did we ever match in our search context?". 
